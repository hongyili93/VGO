package com.vgo.boe;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.vgo.boe.entities.User;
import com.vgo.boe.repositories.UserRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="META-INF/application-context.xml")
public class UserRepostoryTest {
	
	@Autowired
	UserRepository repository;
	@Test
	public void test() {
		User user = new User();
		user.setUserName("UN1");
		user.setLastName("LN1");
		user.setFirstName("FN1");
		user.setEmailAddress("EA1");
		
		repository.save(user);
		
		User dbuser = repository.findOne(user.getUserId());
		assertNotNull(dbuser);
		System.out.println(dbuser.getEmailAddress());
	}

}
