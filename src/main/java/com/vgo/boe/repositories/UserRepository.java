package com.vgo.boe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vgo.boe.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
